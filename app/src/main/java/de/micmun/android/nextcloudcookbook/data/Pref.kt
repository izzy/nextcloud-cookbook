/*
 * Pref.kt
 *
 * Copyright 2020 by MicMun
 */
package de.micmun.android.nextcloudcookbook.data

/**
 * Constants for pref name.
 *
 * @author MicMun
 * @version 1.1, 26.05.20
 */
class Pref {
   companion object {
      const val RECIPE_DIR = "recipe_directory"
      const val THEME = "theme_setting"
   }
}
